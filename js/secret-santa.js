import { LitElement, html, css } from 'lit-element';
import '@material/mwc-top-app-bar-fixed';
import '@material/mwc-icon-button';
import '@material/mwc-icon';
import '@material/mwc-button';
import '@material/mwc-textfield';
import '@material/mwc-list';
import '@material/mwc-list/mwc-check-list-item';

class SecretSanta  extends LitElement {

  static get styles() {
    return css`
    .mwc-top-app-bar-fixed {
        --mdc-theme-primary: orange;
        --mdc-theme-on-primary: black;
      }
    `;
  }

  static get properties() {
    return {
        persona: {
            type: Object
        },
        personas: {
            type: Array,
        }
    };
  }

  constructor() {
    super();
    this.persona={"nombre": "", "regalos": []};
    this.personas=[];
  }

  render() {
    return html`
    <mwc-top-app-bar-fixed prominent dense>
        <mwc-icon-button icon="menu" slot="navigationIcon"></mwc-icon-button>
        <div slot="title">Title</div>
        <mwc-icon-button icon="file_download" slot="actionItems"></mwc-icon-button>
        <mwc-icon-button icon="print" slot="actionItems"></mwc-icon-button>
        <mwc-icon-button icon="favorite" slot="actionItems"></mwc-icon-button>
        <div><!-- content --></div>
    </mwc-top-app-bar-fixed>
 
    <mwc-textfield label="Nombre" class="mdc-text-field__input" type="text" @change="${this.updateNombre}"  .value="${this.persona.nombre}" id="current-persona-nombre"></mwc-textfield>
    <br/><br/>
    <mwc-button 
        id="button"
        @click="${this.agregar}"
        raised
        label="Agregar!"
        ?disabled="${this.persona === ""}"
        >
    </mwc-button>
    
    ${this.personas.length === 0
        ? html` <p> No hay personas </p> `
        : html`
        <mwc-list multi>
            ${this.personas.map(
                (persona) => html`
                    <mwc-list-item graphic="icon">
                        <slot>${persona.nombre}</slot>
                        <br/>
                    </mwc-list-item>
                    <mwc-textfield label="regalo" class="mdc-text-field__input" type="text" @change="${this.updateRegalo}" .value="${this.persona.regalos}" id="current-persona-regalo"></mwc-textfield>
                    <mwc-button id="button" @click="${this.agregarRegalos}" raised label="Agregar Regalos!" ?disabled="${this.persona.regalos === [] }"> </mwc-button>
                    ${this.persona.regalos.length === 0
                    ? html `<p> No hay regalos </p>`
                    : html `
                    <mwc-list multi>
                        ${this.persona.regalos.map(
                            (regalos) => html `
                                <mwc-list-item>
                                    <slot>${persona.nombre.regalos}</slot>
                                </mwc-list-item>
                            `
                        )}
                    </mwc-list>
                    
                    `}

                `
            )}
        </mwc-list>
        `}


        `;
    
      }

  agregar(){
    var nombre = this.shadowRoot.querySelector("#current-persona-nombre");

    this.persona = {nombre : nombre.value};
    console.log(this.persona.nombre)
    console.log(nombre.value);
    this.personas = [...this.personas, this.persona];

    nombre = '';
    
    
  }

  agregarRegalos(){
      var regalo = this.shadowRoot.querySelector("#current-persona-regalo");
      this.persona = [...this.persona.regalos, regalo.value];
      console.log(regalo.value);
      regalo = '';

  }

  updateNombre(e){
      
    this.persona.nombre = e.target.value;
  }

  updateRegalo(e){
      
    this.persona.regalos = e.target.value;
  }

}

customElements.define('secret-santa', SecretSanta);